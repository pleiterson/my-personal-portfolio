import React from 'react';
import '../../styles/components/Curriculo/cursos.css';
import opice from '../../assets/badges/opiceblum.png';
import fgv from '../../assets/badges/fgv.png';
import bradesco from '../../assets/badges/bradesco.png';
import niduu from '../../assets/badges/niduu.png';
import ietec from '../../assets/badges/ietec.jpg';
import tiexames from '../../assets/badges/tiexames.png';
import bizagi from '../../assets/badges/bizagi.png';
import conquer from '../../assets/badges/conquer.jpg';
import alura from '../../assets/badges/alura.png';
import dio from '../../assets/badges/dio.png';
import out from '../../assets/badges/rafaoutsystems.jpg';
import inova from '../../assets/badges/inova.png';
import aec from '../../assets/badges/aec.jpg';

export default function Cursos() {
    return(
        <div className="extracurricular">
            <h2>Cursos e Conhecimentos Adicionais</h2>
            {/* <div className="licensed">
                <img src={} alt=""/>
                <div className="description-licensed">
                    <h4></h4>
                    <span><b></b></span>
                    <span>Emitido em dez. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div> */}
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp SPTech Desenvolvimento Back-end</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em dez. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial BEE6BE67</span>
                    <a href="https://certificates.digitalinnovation.one/BEE6BE67" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp HTML Web Developer</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em nov. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial EEECFA61</span>
                    <a href="https://certificates.digitalinnovation.one/EEECFA61" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={conquer} alt="Escola Conquer"/>
                <div className="description-licensed">
                    <h4>Inteligência Emocional</h4>
                    <span><b>Escola Conquer</b></span>
                    <span>Emitido em nov. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="https://drive.conqueronline.com.br/Certificados/Intelig%C3%AAncia%20Emocional%20Conquer/1605903889539-3462fb03-5285-4fb4-a817-283b9211e6a9.pdf" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={inova} alt="Inova Business School"/>
                <div className="description-licensed">
                    <h4>Full Agile</h4>
                    <span><b>Inova Business School</b></span>
                    <span>Emitido em out. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="https://i.imgur.com/UiqUcsy.jpg" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Especialista em Inovação Digital</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em set. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 4EEBF4C3</span>
                    <a href="https://certificates.digitalinnovation.one/4EEBF4C3" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={out} alt="RafaOutSystems"/>
                <div className="description-licensed">
                    <h4>Semana do Programador 7x OutSystems</h4>
                    <span><b>RafaOutSystems</b></span>
                    <span>Emitido em set. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="https://i.imgur.com/Uy0u2VM.jpg" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Jornada de Formação Inicial de Consultores em Privacidade de Dados</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em ago. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 70305</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=NVRCMlBRVENJNFpIQ081UTRoUU51dz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Transição de Carreira, Elaboração de Propostas e Desenho de Processos com BPMN - Aula V: Jornada de Consultores LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em ago. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 64700</span>
                    <a href="https://www.tiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=dEt3MXpjQm5xbWUwZHZYbUE0UjJwUT09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Mapeamento de Processos - Aula IV: Jornada de Consultores LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em ago. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 63880</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=eUVqN2VoTFZkUk05UnZ3SGV1Rzl2QT09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Estratégias de Consultoria e Mapeamento de Processos - Aula IV: Jornada de Consultores LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em ago. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 61134</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=djBmM2NlQTVZV2VtVVl0aWpnOHN2dz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Desenvolvedor Front-end React.JS</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 54B7799C</span>
                    <a href="https://certificates.digitalinnovation.one/54B7799C" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Desenvolvedor Front-end Angular</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial F3CCF27C</span>
                    <a href="https://certificates.digitalinnovation.one/F3CCF27C" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Desenvolvedor Full-stack Python</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial A90DBBF2</span>
                    <a href="https://certificates.digitalinnovation.one/A90DBBF2" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Desenvolvedor Back-end PHP</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 94447DF3</span>
                    <a href="https://certificates.digitalinnovation.one/94447DF3" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Full-stack Developer Banco Carrefour</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial B96662DD</span>
                    <a href="https://certificates.digitalinnovation.one/B96662DD" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Back-end Developer Carrefour</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial B635F02F</span>
                    <a href="https://certificates.digitalinnovation.one/B635F02F" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Global Full-stack Developer</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial E7A61E1A</span>
                    <a href="https://certificates.digitalinnovation.one/E7A61E1A" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Mapeamento de Processos para adequação à LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 61138</span>
                    <a href="https://www.tiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=aTZBYzBTSzNwQlR3NW1pS3h4Nk9JUT09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Modelos de adequação à LGPD - Aula III: Jornada de Consultores LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 58327</span>
                    <a href="https://www.tiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=bEVrZ0V5b3pYb2ZpY293TVh4enNwdz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>A arte de vender: de vendedor a mestre em persuasão - Aula II: Jornada de Consultores LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 55684</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=dSt1ejNFNDcxQ1hCdHNNQjZQR21Ydz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Primeiros passos para você iniciar a sua atuação como Consultor em Privacidade de Dados (LGPD) -  Aula I: Jornada de Consultores LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jul. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 52774</span>
                    <a href="https://www.tiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=Q0VkcXUyWk92dFNIeC92VmkwZEhQZz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={dio} alt="Digital Innovation One Inc."/>
                <div className="description-licensed">
                    <h4>Bootcamp Become Remote</h4>
                    <span><b>Digital Innovation One Inc.</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial D9C99ED9</span>
                    <a href="https://certificates.digitalinnovation.one/D9C99ED9" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Como Conduzir um Projeto de Consultoria de Adequação à LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 38821</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=VkVlaEkvWFVXNEVlQk9FMEhkaEd4Zz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Ferramentas e Soluções para LGPD</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 39488</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=b2FnMURBNmlyQU9WK3dEenBRSzQvdz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={alura} alt="Alura Cursos Online"/>
                <div className="description-licensed">
                    <h4>Imersão GameDev JavaScript</h4>
                    <span><b>Alura Cursos Online</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="https://i.imgur.com/HfkoTXf.jpg" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={conquer} alt="Escola Conquer"/>
                <div className="description-licensed">
                    <h4>Inteligência Financeira</h4>
                    <span><b>Escola Conquer</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="https://drive.conqueronline.com.br/Certificados/Intelig%C3%AAncia%20Financeira/1594069138356-54271baa-ae16-4b52-978c-0ebffb9a3fc3.pdf" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={bizagi} alt="Bizagi"/>
                <div className="description-licensed">
                    <h4>Modelagem de Processos</h4>
                    <span><b>Bizagi</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="https://i.imgur.com/PxoX1lV.jpg" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>OKR: Um caminho para o Business Agility</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 47636</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=eG9mcXlYenIvZ3dRVVRwKzVGd21PQT09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Profissional de Privacidade de Dados (LGPD)</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 43246</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_profissional.php?COD_INSCRICAO=NnZic0tuR2VxZ3dJRkZyQk1ibXRMUT09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={tiexames} alt="TIexames"/>
                <div className="description-licensed">
                    <h4>Visão prática de um Profissional de Segurança da Informação</h4>
                    <span><b>TIexames</b></span>
                    <span>Emitido em jun. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 45053</span>
                    <a href="https://www.eadtiexames.com.br/novoensino/certificado_evento_pdf.php?COD_INSCRICAO=U2ZkSWM0QW5uRythVFo4Qk4vTjRrdz09" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={ietec} alt="Ietec - Instituto de Educação Tecnológica"/>
                <div className="description-licensed">
                    <h4>Gestão de Processos</h4>
                    <span><b>Ietec - Instituto de Educação Tecnológica</b></span>
                    <span>Emitido em mai. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial ACPkAeui7q</span>
                    <a href="https://virtual.ietec.com.br/" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={niduu} alt="Niduu"/>
                <div className="description-licensed">
                    <h4>Semana da Gamificação</h4>
                    <span><b>Niduu</b></span>
                    <span>Emitido em mai. 2020 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 9mx3j</span>
                    <a href="https://niduu.com/diploma/9mx3j" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={bradesco} alt="Fundação Bradesco"/>
                <div className="description-licensed">
                    <h4>Fundamentos de ITIL</h4>
                    <span><b>Fundação Bradesco</b></span>
                    <span>Emitido em dez. 2019 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial A66C0D04-83F6-4BEF-A8FA-7CB2331E7777</span>
                    <a href="https://www.ev.org.br/validar-certificado" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={fgv} alt="Fundação Getulio Vargas"/>
                <div className="description-licensed">
                    <h4>Gerenciamento do Escopo de Projetos</h4>
                    <span><b>Fundação Getulio Vargas</b></span>
                    <span>Emitido em nov. 2019 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 2685537.368.OCWGPJEAD-01-2009-1</span>
                    <a href="https://imgur.com/mGK5CDn.png" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={fgv} alt="Fundação Getulio Vargas"/>
                <div className="description-licensed">
                    <h4>Como Organizar o Orçamento Familiar</h4>
                    <span><b>Fundação Getulio Vargas</b></span>
                    <span>Emitido em out. 2019 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 2685530.354.OCWOFEAD-0120-11-1</span>
                    <a href="https://i.imgur.com/Pp8CrVk.jpg" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed">
                <img src={opice} alt="Opice Blum Academy"/>
                <div className="description-licensed">
                    <h4>Curso Preparatório para Certificações EXIN - LGPD Essential e GDPR Foundation</h4>
                    <span><b>Opice Blum Academy</b></span>
                    <span>Emitido em ago. 2019 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial 489101AFF73F9B73519950</span>
                    <a href="https://e-certificado.com/login/autenticar?c=489101AFF73F9B73519950" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
            <div className="licensed-last">
                <img src={aec} alt="AeC Centro de Contatos"/>
                <div className="description-licensed">
                    <h4>Treinamento PDCA</h4>
                    <span><b>AeC Centro de Contatos</b></span>
                    <span>Emitido em fev. 2017 ∙ Nenhuma data de expiração</span>
                    <span>Nº da credencial </span>
                    <a href="https://i.imgur.com/kiE6Qk5.jpg" target="_blank" rel="noopener noreferrer">Visualizar credencial</a>
                </div>
            </div>
        </div>
    );
}